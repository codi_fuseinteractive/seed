<?php

/**
 * Base class for our includes which handle some pretty generic data management
 * so we're not writing getters and setters and variables can be defined by
 * implementing classes.
 */
class SeedBase {
  /**
   * Attribute getter. Allows for a get_ATTRIBUTE() method, otherwise falls
   * back to returning the value of the key in the attributes array.
   *
   * @param mixed $key Key of the attribute to get.
   * @return mixed The value.
   */
  public function __get($key) {
    $getter = 'get_'. $key;
    if (is_callable(array($this, $getter))) {
      return $this->$getter();
    }

    if (array_key_exists($key, $this->attributes)) {
      return $this->attributes[$key];
    }
  }

  /**
   * Attribute setter. Allows for a set_ATTRIBUTE() method, otherwise falls
   * back to setting the value of the key in the attributes array.
   *
   * @param mixed $key Key to set.
   * @param mixed $value Value of the key.
   * @return bool Whether or not the set was successful.
   */
  public function __set($key, $value) {
    $setter = 'set_'. $key;
    if (is_callable(array($this, $setter))) {
      $this->$setter($value);
    }

    if (array_key_exists($key, $this->attributes)) {
      $this->attributes[$key] = $value;
    }

    return $this->attributes[$key] === $value;
  }

  /**
   * Handle isset() or empty() calls on attributes.
   *
   * @param mixed $key Key to check.
   * @return bool Whether or not the key has a value.
   */
  public function __isset($key) {
    return isset($this->attributes[$key]);
  }

  /**
   * Handle unset() calls on attributes.
   *
   * @param mixed $key Key to unset.
   */
  public function __unset($key) {
    unset($this->attributes[$key]);
  }
}