<?php

class SeedAliasTest {
  /**
   * Tests the creation and deletion of development (local) aliases.
   */
  public function testDevAlias() {
    $alias = new SeedAlias('test_project', 'test_user');
    $alias->dev_alias('test', 'test', 'test', 'test');

    SeedTest::assert($alias->alias_exists('dev.test_user'), TRUE);

    $alias->delete();

    SeedTest::assert($alias->alias_exists('dev.test_user'), FALSE);
  }

  /**
   * Tests the creation and deletion of remote aliases.
   */
  public function testRemoteAlias() {
    $alias = new SeedAlias('test_project', 'test_user');
    $alias->remote_alias('test', 'test', 'test', 'test', 'test', 'test', FALSE, 'test', 'test', 'test', 'test', 6, 'test', FALSE);

    SeedTest::assert($alias->alias_exists('test'), TRUE);

    $alias->delete();

    SeedTest::assert($alias->alias_exists('test'), FALSE);
  }
}